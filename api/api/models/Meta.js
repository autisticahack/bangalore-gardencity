var uuid = require('node-uuid');

module.exports = {
  attributes: {
  name: {
      type: 'string',
      required: true
    },
    description:{
      type:'string',
      required: true
    },
    type:{
       type: 'string',
       required:true
    },
    content: {
      type: 'string',
      required: true
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.createdAt;
      return obj;
    }
  },
};