var uuid = require('node-uuid');

module.exports = {
  attributes: {
  name: {
      type: 'string',
      required: true
    },
    dictionary: {
      type: 'json',
      required: true,
      defaultsTo: '[]'
    },
    pysicalDictionary: {
      type: 'json',
      required: true,
      defaultsTo: '[]'
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.createdAt;
      return obj;
    }
  },
};