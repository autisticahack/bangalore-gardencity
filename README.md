##About Autistica App

The mobile application is built using reactnative. We have covered two users - child and parent.
The UI has two modules:
* child module - the child module has voice capture feature integrated which enables the user to interact and the text is captured and sent to backend api 
* to process. 
* parent module - The parent module is intended for the parent user to monitor child's behaviour .

* UI and Backend are not integrated. Currently mock data is being used instead of backend services.
* Backend is rest web services.
