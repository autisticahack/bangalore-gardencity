'use strict';

import React, { Component } from 'react';
import Voice from 'react-native-voice';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ToastAndroid,
  TouchableHighlight,
  Navigator
} from 'react-native';


class HowAreYouFeeling extends Component {
	
	constructor(props) {
    super(props);
	
    this.state = {
      recognized: '',
      pitch: '',
      error: '',
      end: '',
      started: '',
      results: [],
      partialResults: [],
	  isButtonPressed: false,	    
      isLeftButtonPressed: false,
      isRightButtonPressed: false
    };
    Voice.onSpeechStart = this.onSpeechStart.bind(this);
    Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this);
    
    Voice.onSpeechError = this.onSpeechError.bind(this);
    Voice.onSpeechResults = this.onSpeechResults.bind(this);
     
  }
   onSpeechStart(e) {
    this.setState({
      started: '√',
    });
  }
  onSpeechRecognized(e) {
    this.setState({
      recognized: '√',
    });
  }

  onSpeechError(e) {
    this.setState({
      error: e.error,
    });
  }
  onSpeechResults(e) {
    this.setState({
      results: e.value,
    });
	this.restPostCall()	
  }

  _startRecognizing(e) {
	 
    this.setState({
      recognized: '',
      pitch: '',
      error: '',
      started: '',
      results: [],
      partialResults: [],
    });
	
    const error = Voice.start('en')
    if (error) {
      ToastAndroid.show(error, ToastAndroid.SHORT);
    }
	
  }

	render () {
    var message = 'How are you feeling ?';

	 return (
            <View style={styles.container}>            
                <View style={{height: 90, backgroundColor: '#\D6A2DC'}}>
                </View>
              
                 <Text style={styles.welcome}>
                        {message}
                  </Text>
                    
                           <TouchableHighlight 
                              onPress={this._startRecognizing.bind(this)}
                              style={styles.container2}>
                                 <Image
                                    style={styles.logo}
                                    source={require('./images/emoticons/MicButton.png')}
                                  />
                           </TouchableHighlight>
						   
						    <Text
							 style={styles.stat}>
							  {this.state.results[0]}
							</Text>
                        
                       <View style={{flex: 1, flexDirection: 'row'}}>                          

                          <TouchableHighlight 
                              onPress={this.onLeftButtonPressed.bind(this)}
                              style={styles.smile}>
                                 <Image
                                    style={styles.Arrowlogo}
                                    source={require('./images/emoticons/LeftButton.png')}
                                  />
                           </TouchableHighlight>

                          <TouchableHighlight 
                              onPress={this.onRightButtonPressed.bind(this)}
                              style={styles.sad}>
                                 <Image
                                    style={styles.Arrowlogo}
                                    source={require('./images/emoticons/RightButton.png')}
                                  />
                           </TouchableHighlight>

                      </View>
   
                   
             </View> 
    );
  
	}

  onRecordIconPressed(){       
      ToastAndroid.show('Record !!', ToastAndroid.SHORT); 
      this.setState({isButtonPressed: true});
  }

  onLeftButtonPressed(){       
      ToastAndroid.show('Left !!', ToastAndroid.SHORT); 
      this.setState({isLeftButtonPressed: true});
  }

  onRightButtonPressed(){       
      ToastAndroid.show('Right !!', ToastAndroid.SHORT); 
      this.setState({isRightButtonPressed: true});
  }
 restPostCall(){
	
	
	 var myRequest = new Request(
                                  'http://192.168.0.158:1337/records', {
                                          method: 'POST',
                                          headers: {
                                            'Accept': 'application/json',
                                            'Content-Type': 'application/json',
                                          },
                                          body:'{"assessment": {"text": "'+this.state.results[0]+'"},"isSolutionDone": false}' 
										  
                                          }); 
	fetch(myRequest)
          .then(function(response) {
              if(response.status == 200) return;
              else throw new Error('Something went wrong on api server!');
          })
          .then(function(response) {
              ToastAndroid.show('', ToastAndroid.SHORT);
          })
          .catch(function(error) {
              console.error(error);
          });
}
};  


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: '#FCECFC',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    paddingTop: 30
  },
  logo: {
    marginTop: 20,
    width: 100,
    height: 100,
    paddingTop: 10,
  },
  Arrowlogo: {
    width: 60,
    height: 60,
     marginTop: 20,
         paddingTop: 10
  },

  DairyButtonText: {
    color: '#fff',
    fontSize: 20,
    alignSelf: 'center',
},

   sad: {
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 20,
    paddingTop: 10,
    marginRight: 40, 
    alignItems: 'flex-end',
    backgroundColor: '#FCECFC',
  },
    container2: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 20,
    paddingTop: 10,
    alignItems: 'center',
    backgroundColor: '#FCECFC',
  },
  smile: {
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 20,
    paddingTop: 10,
    marginLeft: 40, 
    alignItems: 'flex-start',
    backgroundColor: '#FCECFC',
  }

});


module.exports = HowAreYouFeeling;