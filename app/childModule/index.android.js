import React, { Component } from 'react';
import Voice from 'react-native-voice';
import {
  AppRegistry,
  ToastAndroid
} from 'react-native';

var RecordScreen = require('./RecordScreen');

export default class AwesomeProject extends Component {

  constructor(props){
    super(props);

    this.state = {
      isLoggedIn: false
    };
  }
  
  render() {   
         
        return (    
          <RecordScreen onRecord={this.onRecord} />   
        );
    }

    onRecord() {
      ToastAndroid.show('Login Succesful!!', ToastAndroid.SHORT);       
    }
  };

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
