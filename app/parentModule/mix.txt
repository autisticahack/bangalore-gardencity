import React, { Component } from 'react';
import { AppRegistry,Image, Text, View,StyleSheet,TouchableHighlight } from 'react-native';

class ParentProject extends Component { 
  constructor(props) {
   
    super(props);
    var text='';
    var hours = new Date().getHours();
    var hello="";
    if(hours < 12){
      hello = "Good Morning";
    }else if(hours < 16){
      hello = "Good Afternoon";
    }else{
      hello = "Good Evening";
    }
     this.state = {    
                  hi:hello,
                   showView: true,
                 };   
  } 
   _startRecognizing(e) {
  
   var data1 = 60;
    var data2 = 80;
    var data3 = 80;
    var data4 = 80; 
    var data5 = 50;
    var total = data1 + data2 + data3 + data4 + data5;
    var percentSad = total/5;
    var percentHappy = (500-total)/5;
    var msg="";
    if(percentHappy < 30){
       msg = "Happyness level is "+percentHappy+"%. Suggest you to have a look into the guide. "
    }else if(percentHappy < 50){
      msg = "Happyness level is "+percentHappy+"%. Suggest you to have a look into the guide. "
    }else{
      msg = "Happyness level is "+percentHappy+"%. Kid is doing great. "
    }

   this.setState({
                  behaviourLevelSad: percentSad+"%",
                  behaviourLevelHappy: percentHappy+"%",
                  message: msg,

                   showView: false,
   });

   }
  render() {
 
if (this.state.showView) {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
         
      }}>

        <View style={{alignItems:'strech', width: 250, height: 90,backgroundColor: '#82E1E2'}} >
        <Text style={styles.titleText}>{this.state.hi}</Text> 
        </View>
        <View style={{alignItems:'strech',width: 250, height: 90,backgroundColor: '#E2F5F5'}} />
        <View style={{alignItems:'strech',width: 250, height: 350, flexDirection: 'row', backgroundColor: '#E2F5F5', justifyContent: 'space-around',}} >
          <TouchableHighlight 
             	onPress={this._startRecognizing.bind(this)}
             	style={styles.button}> 
          <View style={{width: 100, height: 100, backgroundColor: '#82E1E2'}}>
             <Text style={styles.titleText}>View Behaviour</Text>   
           </View>  
          </TouchableHighlight>
          <View style={{width: 100, height: 100, backgroundColor: '#82E1E2'}}>
             <Text style={styles.titleText}>Guide</Text>  
          </View>
        </View>

      </View>

    );
   }
   if(!this.state.showView){
    return (
     
      <View style={{
        flex: 1,
        flexDirection: 'column',
      }}>

        <View style={{alignItems:'strech',width: 250, height: 90,backgroundColor: '#82E1E2'}} >
          <Text style={styles.titleText}>{this.state.hi}</Text> 
        </View>
     
        <View style={{alignItems:'strech',width: 250, height: 35,backgroundColor: '#E2F5F5'}}>
          
        </View>
        <View style={{alignItems:'strech',width: 250, height: 285,alignItems:'center', flexDirection: 'column', justifyContent: 'center', backgroundColor: '#E2F5F5'}}>
          <View id="behaviourHappy" style={{borderRadius:4,width: 40, height: this.state.behaviourLevelSad, backgroundColor: '#8AC155'}}/> 
      
          <View id="behaviourSad" style={{borderRadius: 10,width: 40, height: this.state.behaviourLevelHappy, backgroundColor: '#FF0000'}}/>
            
        </View>
        <View style={{alignItems:'strech',width: 250, height: 35,backgroundColor: '#82E1E2'}} >
          <Text style={styles.message}>{this.state.message}</Text> 
        </View>
      </View>
    );   
   }
  }
     
  componentDidMount(){
  
 
  }

};
const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontFamily: 'cambria',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop:20,
    color:'#E8FAFB',
  },
  message: {
      fontFamily: 'cambria',
     textAlign: 'center',
    color:'#3258FB',  
  },
});
AppRegistry.registerComponent('ParentProject ', () => ParentProject );