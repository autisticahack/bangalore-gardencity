import React, { Component } from 'react';
import { AppRegistry,Image, Text, View,StyleSheet,TouchableHighlight } from 'react-native';



var Behaviour=require('./Behaviour');
var ParentGuide =require('./parentGuide');


class ParentWelcome extends Component { 
  constructor(props) {
    super(props);
    var hours = new Date().getHours();
    var hello="";
    if(hours < 12){
      hello = "Good Morning";
    }else if(hours < 16){
      hello = "Good Afternoon";
    }else{
      hello = "Good Evening";
    }
    
    this.state = {
                  hi:hello,
                  showParentGuide:false,
                  showBehaviour:false,
                 };
  } 

_loadBehaviour(e){

  this.setState({
    showBehaviour:true,
    showParentGuide:false,
  });


}
_loadGuide(e){
this.setState({
    showBehaviour:false,
    showParentGuide:true,
  });

}
  render() {
    
    var chk=this.state.showBehaviour;
    if(chk){
      return(
              <Behaviour/>
    );
    }
    if(this.state.showParentGuide){
      return(
              <ParentGuide/>
    );
    }

      return (

          <View style={{alignItems:'stretch',flex: 1,flexDirection: 'column',}}>

              <View style={{width: 450, height: 90,backgroundColor: '#82E1E2'}} >
                <Text style={styles.titleText}>{this.state.hi}</Text> 
              </View>
              <View style={{width: 450, height: 90,backgroundColor: '#E2F5F5'}} />
              <View style={{width: 450, height: 500, flexDirection: 'row', backgroundColor: '#E2F5F5', justifyContent: 'space-around',}} >
                  <TouchableHighlight onPress={this._loadBehaviour.bind(this)} style={styles.button}>
                      <View style={{width: 100, height: 100, backgroundColor: '#82E1E2'}}>
                          <Text style={styles.titleText}>View Behaviour</Text>   
                       </View>
                  </TouchableHighlight> 
                  <TouchableHighlight onPress={this._loadGuide.bind(this)} style={styles.button}>
                  <View style={{width: 100, height: 100, backgroundColor: '#82E1E2'}}>
                       <Text style={styles.titleText}>Guide</Text>  
                  </View>
                  </TouchableHighlight> 
              </View>


           </View>
        );


    
    
  }

};
const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontFamily: 'cambria',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop:20,
    color:'#E8FAFB',
  },
});
AppRegistry.registerComponent('ParentProject', () => ParentWelcome);